package com.demo.task.domain;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    private String id;
    private String deviceUuid;
    private String pid;
    private String model;
    private String scope;
    private String value;
    private Long generatedTime;
    private Long uploadedTime;
}
