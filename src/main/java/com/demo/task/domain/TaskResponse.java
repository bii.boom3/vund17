package com.demo.task.domain;

import com.demo.task.domain.Task;
import com.demo.task.entity.TaskEntity;
import lombok.*;

import java.util.List;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TaskResponse {
    private List<TaskEntity> results;
    private int offset;
    private int totalCount;
}
