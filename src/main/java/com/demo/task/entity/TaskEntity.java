package com.demo.task.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "task")
@Builder
public class TaskEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;
    @Column(name = "device_uuid")
    private String deviceUuid;
    @Column(name = "pid")
    private String pid;
    @Column(name = "model")
    private String model;
    @Column(name = "scope")
    private String scope;
    @Column(name = "value")
    private String value;
    @Column(name = "generatedTime")
    private Long generatedTime;
    @Column(name = "uploadedTime")
    private Long uploadedTime;
}
