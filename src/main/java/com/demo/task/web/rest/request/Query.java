package com.demo.task.web.rest.request;

import lombok.*;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Query {
    private String deviceUuid;
    private Set<String> scopes;
}
