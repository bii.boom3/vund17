package com.demo.task.web.rest.request;

import lombok.*;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TaskFilter {
    private Set<Query> queries;
    private TimeQuery time;
    private int maxCount;
    private int offset;
}
