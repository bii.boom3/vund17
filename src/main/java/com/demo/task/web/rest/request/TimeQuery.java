package com.demo.task.web.rest.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TimeQuery {
    private Long startTime;
    private Long endTime;
}
