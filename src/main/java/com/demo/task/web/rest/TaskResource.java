package com.demo.task.web.rest;

import com.demo.task.domain.Task;
import com.demo.task.web.rest.request.TaskFilter;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/task")
public interface TaskResource {
    @PostMapping("")
    List<Task> filter(@Valid @RequestBody TaskFilter taskFilter) throws Exception;
}
