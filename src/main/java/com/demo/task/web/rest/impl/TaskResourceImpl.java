package com.demo.task.web.rest.impl;

import com.demo.task.domain.Task;
import com.demo.task.service.TaskService;
import com.demo.task.web.rest.TaskResource;
import com.demo.task.web.rest.request.TaskFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class TaskResourceImpl implements TaskResource {
    private final TaskService taskService;
    @Autowired
    public TaskResourceImpl(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public List<Task> filter(TaskFilter taskFilter) throws Exception {
        return taskService.syncTask(taskFilter);
    }
}
