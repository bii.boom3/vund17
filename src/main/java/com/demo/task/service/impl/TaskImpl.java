package com.demo.task.service.impl;

import com.demo.task.domain.Task;
import com.demo.task.domain.TaskResponse;
import com.demo.task.repository.TaskRepository;
import com.demo.task.service.TaskService;
import com.demo.task.service.mapper.TaskMapper;
import com.demo.task.util.Const;
import com.demo.task.web.rest.request.TaskFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TaskImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    public TaskImpl(TaskRepository taskRepository, TaskMapper taskMapper) {
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }

    @Override
    public List<Task> syncTask(TaskFilter req) throws IOException {
        System.out.println(getResponse(req));;
        var result = getResponse(req);
        var resp = new ObjectMapper().readValue(result, TaskResponse.class);
        var taskEntities = resp.getResults();

        var data = taskRepository.saveAll(taskEntities);
        return taskMapper.toTarget(data);
    }
    private String getResponse(TaskFilter query) throws IOException {
        URL obj = new URL(Const.GET_URL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("X-ND-TOKEN", "ip0E2z7COsxH8KPD3AT8oKDymJaugRiWS+or/Le");
        con.setRequestProperty("Content-Type", "application/json");
        String reqJson = new ObjectMapper().writeValueAsString(query);
        System.out.println(reqJson);
        try (OutputStream os = con.getOutputStream()) {
            byte[] input = reqJson.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        try (
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine);
            }
            return response.toString();
        }
    }
}
