package com.demo.task.service;

import com.demo.task.domain.Task;
import com.demo.task.web.rest.request.TaskFilter;

import java.io.IOException;
import java.util.List;

public interface TaskService {
    List<Task> syncTask(TaskFilter req) throws IOException;
}
