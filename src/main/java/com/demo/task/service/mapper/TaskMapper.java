package com.demo.task.service.mapper;

import com.demo.task.domain.Task;
import com.demo.task.entity.TaskEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TaskMapper extends ModelMapper<TaskEntity, Task> {
}
