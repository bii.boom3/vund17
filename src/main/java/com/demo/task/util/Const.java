package com.demo.task.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
public interface Const {
    String GET_URL = "https://ioeapi.nextdrive.io/v1/device-data/query";
}
