package com.demo.task;

import com.demo.task.service.TaskService;
import com.demo.task.web.rest.request.TaskFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan({"com.demo.task.*"})
@EntityScan(basePackages = {"com.demo.task.*"})
@EnableScheduling
@EnableCaching

public class TaskBEApplication implements CommandLineRunner {
    @Autowired
    private TaskService service;
    public static void main(String[] args) {
        SpringApplication.run(TaskBEApplication.class, args);
    }

    @Override
    public void run(String... args) throws IOException {
        var params = "{\"queries\": [{\"deviceUuid\":\"29eecea0-faee-4fc6-9f0d-2b78e0fcec97\",\"scopes\":[\"normalUsage\"]}],\"time\":{\"startTime\":1670716800000,\"endTime\":1670803200000},\"maxCount\":10,\"offset\":0}";
        var taskFilter = new ObjectMapper().readValue(params, TaskFilter.class);
        service.syncTask(taskFilter);
    }
}
